import sys
import matplotlib.pyplot as plt
import numpy as np
import torch
from torchsummary import summary
import datahandler
import train_seg
import seg_model
from constants import EPOCHS, MODEL_NAME, INPUT_DIM, MODEL_NAMES
from train import train
from model import get_model
from evaluation import metrics, confMtrx
import seg_classifier_model


def main(model_name = MODEL_NAME):
    model = get_model(model_name, 7, False)
    train_loader, val_loader = datahandler.getHamDataLoaders()
    train(train_loader, val_loader, model, EPOCHS)
    print("\n\n -------------Done----------------")

def seg_main():
    train_loader, val_loader = datahandler.getHamDataLoadersSeg()
    model = seg_model.SegmentationModel([3,16,32,64,1],[False, True, True])
    #summary(model, (3,128,128), batch_size=16,device="cpu")
    model.load_state_dict(torch.load(f"Models/segmentation{INPUT_DIM}skip.pt"))
    train_seg.train_epochs(model, train_loader, val_loader, EPOCHS)

def seg_main_classifier():
    model = seg_classifier_model.get_model("Models/densenet121_run20.pt", f"Models/segmentation{INPUT_DIM}skip.pt")
    train_loader, val_loader = datahandler.getHamDataLoaders()
    train_seg.train_epochs_classifier(model, train_loader, val_loader, EPOCHS)

def eval_main(seg, model_run="densenet121_run20"):
    if seg:
        model = seg_classifier_model.get_model_trained("Models/SegClassifier/classifier.pt", "Models/SegClassifier/seg.pt")
        model.to("cuda")
    else:
        model = get_model("densenet121", 7, False)
        model.load_state_dict(torch.load(f"Models/{model_run}.pt"))
        model.to("cuda")
    train_loader, val_loader = datahandler.getHamDataLoaders()        
    #Evaluation: Uncomment below
    metrics(model, val_loader)
    confMtrx(model, train_loader, val_loader)

# When run from command line it can take an additional argument:
# if you add the additional argument with parameter 'seg' then it'll run the segmentation training loop
# otherwise it'll just run the main model training loop
if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == "seg":
            print("SEGMENTATION")
            seg_main()
        if sys.argv[1] == "segc":
            print("SEGMENTATION CLASSIFIER")
            seg_main_classifier()
        if sys.argv[1] == "eval":
            print("EVALUATION")
            if sys.argv[2] == "seg":
                eval_main(True)
            else:
                eval_main(False)
        if sys.argv[1] == "main":
            print("NORMAL")
            main(MODEL_NAMES[int(sys.argv[2])])
        else:
            print("NORMAL")
            main()
    else:
        print("NORMAL")
        main()
#FYI training samples for all but the 'melanocytic nevi' have been equalised (samples duplicated) so they're an even distribution
# Before and after prints of the tables are uncommented to show (lines 142 and 154 in datahandler)