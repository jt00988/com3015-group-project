EPOCHS = 16
BATCH_SIZE = 32
VAL_SPLIT = 2000
TEST_SPLIT = 500

# Model name can be one of the following:
# resnet50
# vgg11
# densenet121
# inception
MODEL_NAMES = ["resnet50","vgg11","densenet121","inception"]
MODEL_NAME = "densenet121"
INPUT_DIM = 224 # leave as 224 for all models except inception