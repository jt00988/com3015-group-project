import os, cv2
import pandas as pd
from glob import glob

import torch
from PIL import Image
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import numpy as np
from tqdm import tqdm

from constants import BATCH_SIZE, INPUT_DIM, VAL_SPLIT

data_dir = 'Data/HAM10000'
lesion_type_dict = {
    'akiec': 'Actinic keratoses',
    'bcc': 'Basal cell carcinoma',
    'bkl': 'Benign keratosis-like lesions',
    'df': 'Dermatofibroma',
    'mel': 'dermatofibroma',
    'nv': 'Melanocytic nevi',
    'vasc': 'Vascular lesions'
}

# returns trainLoader, valLoader
def getHamDataLoaders():
    # Firstly returns two dataframes, one with all images and another with just the lesion_ids containing a single image (for valuating models)
    df_ham, df_ham_single_image = setupHamData()

    # Then returns two dataframes, df_ham_val is a 20% split of all single images, df_ham_train is the rest
    df_ham_train, df_ham_val = createHamTrainVal(df_ham, df_ham_single_image)

    all_image_path = getHamImagePaths()
    #norm_mean, norm_std = normalisation(all_image_path) #norm_std causing darkness
    norm_mean = 0.5
    norm_std = 0.25

    # Define the training set using the table train_df and using our defined transitions (train_transform)
    train_transform, val_transform = getTrainValTransform(INPUT_DIM, norm_mean, norm_std)
    training_set = HAM10000(df_ham_train, transform=train_transform)
    trainLoader = DataLoader(training_set, batch_size=BATCH_SIZE, shuffle=True, num_workers=4)

    # Same for the validation set:
    validation_set = HAM10000(df_ham_val, transform=val_transform)
    valLoader = DataLoader(validation_set, batch_size=BATCH_SIZE, shuffle=False, num_workers=4)


    return trainLoader, valLoader


def getHamDataLoadersSeg():

    # Firstly returns two dataframes, one with all images and another with just the lesion_ids containing a single image (for valuating models)
    df_ham, df_ham_single_image = setupHamData()

    # Then returns two dataframes, df_ham_val is a 20% split of all single images, df_ham_train is the rest
    df_ham_train, df_ham_val = createHamTrainVal(df_ham, df_ham_single_image)

    # Define the training set using the table train_df and using our defined transitions (train_transform)
    augment_transform, val_transform = getTrainValTransform(INPUT_DIM, 0.5, 0.25, True)
    mask_transform = getMaskTransform(INPUT_DIM)
    training_set = HAM10000Seg(df_ham_train, x_transform=augment_transform, y_transform=mask_transform)
    trainLoader = DataLoader(training_set, batch_size=BATCH_SIZE, shuffle=True, num_workers=4)

    # Same for the validation set:
    validation_set = HAM10000Seg(df_ham_val, x_transform=val_transform, y_transform=mask_transform)
    valLoader = DataLoader(validation_set, batch_size=BATCH_SIZE, shuffle=False, num_workers=4)

    return trainLoader, valLoader

# Excludes rows that are in the val set
# Identifies if an image is part of the train or val
def get_val_rows(x):
    # list of lesion_id's in val set
    val_list = list(df_ham_val['image_id'])
    if str(x) in val_list:
        return 'val'
    else:
        return 'train'

def getFilesInHamDirectory():
    # print("Files in directory: ", os.listdir("Data/HAM10000"))
    return os.listdir(data_dir)

def getHamImagePaths():
    all_image_path = glob(os.path.join(data_dir, '*', '*.jpg'))
    # print("Basename = ", os.path.basename(all_image_path[0]))
    # print("Split text = ", os.path.splitext(os.path.basename(all_image_path[0]))[0])
    return all_image_path

def normalisation(all_image_path):
    img_h, img_w = 224, 224
    imgs = []
    means, standevs = [], []

    for i in tqdm(range(len(all_image_path))):
        img = cv2.imread(all_image_path[i])
        img = cv2.resize(img, (img_h, img_w))
        imgs.append(img)

    imgs = np.stack(imgs, axis=3)
    imgs = imgs.astype(np.float32) / 255

    for i in range(3):
        pixels = imgs[:, :, i, :].ravel()
        means.append(np.mean(pixels))
        standevs.append(np.std(pixels))

    means.reverse()
    standevs.reverse()

    print("normMean = {}".format(means))
    print("normStd = {}".format(standevs))
    return means, standevs

def setupHamData():
    image_id_path_dict = {
        os.path.splitext(os.path.basename(x))[0]: x for x in getHamImagePaths()
    }

    # Create HAM10000 dataframe combining existing metadata and cell types
    df_ham = pd.read_csv(os.path.join(data_dir, 'HAM10000_metadata.csv'))
    df_ham['path'] = df_ham['image_id'].map(image_id_path_dict.get)
    df_ham['cell_type'] = df_ham['dx'].map(lesion_type_dict.get)
    df_ham['cell_type_idx'] = pd.Categorical(df_ham['cell_type']).codes
    # print(df_ham.head())
    # print(df_ham[['lesion_id', 'localization']].to_string(index=False))

    df_ham, df_ham_single_image = filterHamDuplicates(df_ham)

    return df_ham, df_ham_single_image

def filterHamDuplicates(df_ham):
    # Image counts (image_id col) associated with each lesion_id
    df_ham_single_image = df_ham.groupby('lesion_id').count()
    # print(df_ham_single_image)
    # Keep only the lesions id's with one image associated
    df_ham_single_image = df_ham_single_image[df_ham_single_image['image_id'] == 1]
    df_ham_single_image.reset_index(inplace=True)
    # print("After removing lesions with multiple images: \n", df_ham_single_image)

    # create copy of the lesion_id column
    df_ham['sing/multi'] = df_ham['lesion_id']

    unique_list = list(df_ham_single_image['lesion_id'])
    #print(unique_list)

   # print(df_ham)

    count = 0
    for x in df_ham['sing/multi']:
        #print(x)
        if x in unique_list:
            df_ham.iloc[count, df_ham.columns.get_loc('sing/multi')] = 'single'
            #print((df_ham.iloc[count, df_ham.columns.get_loc('sing/multi')]), "\t single")
            count+=1
        else:
            df_ham.iloc[count, df_ham.columns.get_loc('sing/multi')] = 'multi'
            #print((df_ham.iloc[count, df_ham.columns.get_loc('sing/multi')]), "\t multi")
            count+=1

    #print("\n df_ham After: \n", df_ham['sing/multi'], "\n\n")
    #print("\n df_ham After: \n", df_ham, "\n\n")


    #print(df_ham[['lesion_id', 'localization', 'path']].to_string(index=False))

    # filter out images that don't have multi's - keep lesions with only single images, remove multi's
    df_ham_single_image = df_ham['sing/multi'] == 'single'
    #print("Filter Step 1: \n", df_ham_single_image)
    df_ham_single_image = df_ham[df_ham['sing/multi'] == 'single']
    #print("Filter Step 2: \n", df_ham_single_image)
    #print(df_ham_single_image.shape)
    #print("\n\nSingle Image DF - Categories and Counts: ", df_ham_single_image[['cell_type', 'cell_type_idx']].value_counts(), "\n\n")

    return df_ham, df_ham_single_image

def equalSampling(df_ham_train):
    # Copy fewer class to balance the number of 7 classes
    data_aug_rate = [15, 10, 5, 50, 0, 40, 5]
    tmp = []
    old_df_ham_train = df_ham_train
    #print("\nPre Concat Table: \n", old_df_ham_train)
    print("\nPre Equalised Train Counts: ", old_df_ham_train[['cell_type', 'cell_type_idx']].value_counts(), "\n")

    for i in range(7):
        if data_aug_rate[i]:
            new_df_ham_train = pd.concat(
                [df_ham_train.loc[df_ham_train['cell_type_idx'] == i, :]] * (data_aug_rate[i] - 1),
                ignore_index=True)
            concat_df_ham_train = pd.concat([old_df_ham_train, new_df_ham_train], ignore_index=True)
            old_df_ham_train = concat_df_ham_train
            #print("\nConcat ", i , " samples: ", concat_df_ham_train[['cell_type', 'cell_type_idx']].value_counts(), "\n")

    #print("\n---Printing Final Below---")
    print("\nEqualised Train Counts: ", concat_df_ham_train[['cell_type', 'cell_type_idx']].value_counts(), "\n")
    #print("\nConcat Table: \n", concat_df_ham_train)



    return concat_df_ham_train

def createHamTrainVal(df_ham, df_ham_single_image):
    # create a val set knowing none of the images have multi in the train set
    y = df_ham_single_image['cell_type_idx']

    # df_ham_val is 20% of the df_ham_single
    _, val_list = train_test_split(list(df_ham_single_image['image_id']), test_size=VAL_SPLIT, random_state=101, stratify=y)

    # identify train and val rows
    # create a new colum to hold if it's in tain of val
    df_ham['train_or_val'] = df_ham['image_id']

    count = 0
    for x in df_ham['image_id']:
        if x in val_list:
            df_ham.iloc[count, df_ham.columns.get_loc('train_or_val')] = 'val'
            count += 1
        else:
            df_ham.iloc[count, df_ham.columns.get_loc('train_or_val')] = 'train'
            count += 1

    # filter out the validation rows
    df_ham_train = df_ham[df_ham['train_or_val'] == 'train']
    # filter out training rows
    df_ham_val = df_ham[df_ham['train_or_val'] == 'val']

    # Equilising training samples
    df_ham_train = equalSampling(df_ham_train)

    # Option to split the test set again in a validation set and a true test set:
    # df_ham_val, df_test = train_test_split(df_ham_val, test_size=0.5)

    #reset indexes after filtering and oversampling
    df_ham_train = df_ham_train.reset_index()
    df_ham_val = df_ham_val.reset_index()

    print(f"""
    Length val: {df_ham_val.shape[0]}
    Length train: {df_ham_train.shape[0]}
    """)

    # df_test = df_test.reset_index()
    #print("\ndf_ham_val Reset Table: \n", df_ham_val)

    return df_ham_train, df_ham_val

def getTrainValTransform(input_size, norm_mean, norm_std, seg=False):
    # define the transformation of the train images.
    train_transform = None
    if seg:
        train_transform = transforms.Compose([
            transforms.Resize((input_size, input_size)),
            transforms.ColorJitter(brightness=0.1, contrast=0.1, hue=0.1),
            transforms.ToTensor(),
            transforms.Normalize(norm_mean, norm_std)
        ])
    else:
        train_transform = transforms.Compose([
            transforms.Resize((input_size, input_size)),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.RandomRotation(20),
            transforms.ColorJitter(brightness=0.1, contrast=0.1, hue=0.1),
            transforms.ToTensor(),
            transforms.Normalize(norm_mean, norm_std)
        ])

    # define the transformation of the val images.
    val_transform = transforms.Compose([
        transforms.Resize((input_size, input_size)), 
        transforms.ToTensor(),
        transforms.Normalize(norm_mean, norm_std)
    ])
    return train_transform, val_transform

def threshold_tensor(x, threshold=0.5):
    return (x > threshold).float()

def getMaskTransform(input_size):
    return transforms.Compose([
        transforms.Resize((input_size, input_size)),
        transforms.ToTensor(),  # Convert the PIL image to a PyTorch tensor
        transforms.Lambda(threshold_tensor)
    ])

# Define a pytorch dataloader for this dataset
class HAM10000(Dataset):
    from PIL import Image
    import torch
    def __init__(self, df, transform=None):
        self.df = df
        self.transform = transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        # Load data and get label
        X = Image.open(self.df['path'][index])
        y = torch.tensor(int(self.df['cell_type_idx'][index]))

        if self.transform:
            X = self.transform(X)

        return X, y

# Define a pytorch dataloader for this dataset
class HAM10000Seg(Dataset):
    def __init__(self, df, x_transform=None, y_transform=None):
        self.df = df
        self.x_transform = x_transform
        self.y_transform = y_transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        # Load data and get label
        X = Image.open(self.df['path'][index])
        y = Image.open(f"{data_dir}/HAM10000_segmentations_lesion_tschandl/{self.df['image_id'][index]}_segmentation.png",formats=['PNG']).convert('L')
        if self.x_transform:
            X = self.x_transform(X)
        if self.y_transform:
            y = self.y_transform(y)
        return X, y
