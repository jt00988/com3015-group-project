# COM3015 Group Project


## Setup

### Retrieving HAM10000 dataset

Download the dataset from: [https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/DBW86T], as well as the segmentation data which can also be found here

Once downloaded, you can add it to the Data/HAM10000 folder. If this is done correctly, you should have the following structure for your data files:

Data/HAM10000/
- HAM10000_images_part_1
- HAM10000_images_part_2
- HAM10000_segmentations_lesions_tschandl

### Run the code

To run, update the constants.py file as needed first, then use one of the following commands:

` python main.py ` - This is for running the image classifier

` python main.py seg ` - This is for running the segmentation model

` python main.py segc ` - This is for running the segmentation classifier (which can be done after the two above have been done)