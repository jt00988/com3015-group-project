import torch
import torch.nn as nn
from torchvision import models

from constants import INPUT_DIM
from seg_model import SegmentationModel

class SegClassifier(nn.Module):
    def __init__(self, seg_model, classifier, train_seg = False):
        super(SegClassifier, self).__init__()
        self.seg_model = seg_model
        self.classifier = classifier
        self.train_seg = True, train_seg
        #modifiy classifier first layer not add 4th input channel
        new_conv = nn.Conv2d(4, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        self.classifier.features.conv0 = new_conv

    def forward(self, x):
        if self.train_seg:
            seg_mask = self.seg_model(x)
        else:
            with torch.no_grad():
                seg_mask = self.seg_model(x)
        x = torch.cat([x,seg_mask],1)
        return self.classifier(x)

    def save(self):
        torch.save(self.seg_model.state_dict(), "Models/SegClassifier/seg.pt")
        torch.save(self.classifier.state_dict(), "Models/SegClassifier/classifier.pt")

    def load(self, classifier_path: str, seg_path: str):
        self.seg_model.load_state_dict(torch.load(seg_path))
        self.classifier.load_state_dict(torch.load(classifier_path))

def get_model_trained(classifier_path: str, seg_path: str):
    classifier = models.densenet121(pretrained=True)
    in_features = classifier.classifier.in_features
    classifier.classifier = nn.Linear(in_features, 7)
    seg_model = SegmentationModel([3,16,32,64,1],[False, True, True])
    model = SegClassifier(seg_model, classifier)
    model.load(classifier_path,seg_path)
    return model

def get_model(classifier_path: str, seg_path: str):
    classifier = models.densenet121(pretrained=True)
    in_features = classifier.classifier.in_features
    classifier.classifier = nn.Linear(in_features, 7)
    classifier.load_state_dict(torch.load(classifier_path))
    seg_model = SegmentationModel([3,16,32,64,1],[False, True, True])
    seg_model.load_state_dict(torch.load(seg_path))
    model = SegClassifier(seg_model, classifier)
    return model