import torch
from torch import nn, optim
from tqdm import tqdm

from torch.autograd import Variable
from constants import MODEL_NAME

# An array of arrays containing the loss and accuracy of each epoch
train_stats = []
val_stats = []

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

criterion = nn.CrossEntropyLoss().to(device)

class AverageMeter(object):
    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def train(train_loader, val_loader, model, num_epochs):
    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    best_validation_loss = 999999 # arbitrarily large number

    for epoch in range(1, num_epochs+1):
        print(f"EPOCH {epoch}")
        print(f"-" * 40)
        loss_train, acc_train = train_epoch(train_loader, model, optimizer, epoch)
        print(f"Training   | Loss {loss_train} | Accuracy {acc_train:.3%}")
        loss_val, acc_val = validate(val_loader, model, epoch)
        print(f"Validation | Loss {loss_val} | Accuracy {acc_val:.3%}")
        
        if loss_val < best_validation_loss:
            best_validation_loss = loss_val
            print(f"\tNEW BEST | LOSS {loss_val} | ACCURACY {acc_val:.3%}")
            torch.save(model.state_dict(), f"Models/{MODEL_NAME}_run{num_epochs}.pt")
        
        print(f"-" * 40 + "\n")

    return train_stats, val_stats

def train_epoch(train_loader, model, optimizer, epoch_num):
    model.train()
    model.to(device)

    train_loss = AverageMeter()
    train_accuracy = AverageMeter()

    loop = tqdm(train_loader, desc="\tTraining", ncols=80, mininterval=0.1)
    for images, labels in loop:
        images, labels = Variable(images).to(device), Variable(labels).to(device)
        N = images.size(0)

        optimizer.zero_grad()
        outputs = model(images)

        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        prediction = outputs.max(1, keepdim=True)[1]

        train_accuracy.update(prediction.eq(labels.view_as(prediction)).sum().item() / N)
        train_loss.update(loss.item())

        loop.set_postfix(loss=loss.item(), accuracy=train_accuracy.avg)

    train_stats.append([train_loss.avg, train_accuracy.avg])
    return train_loss.avg, train_accuracy.avg

def validate(val_loader, model, epoch_num):
    model.eval()
    val_loss = AverageMeter()
    val_acc = AverageMeter()

    with torch.no_grad():

        loop = tqdm(val_loader, desc="\tValidation", ncols=80, mininterval=0.1)
        for images, labels in loop:
            images, labels = Variable(images).to(device), Variable(labels).to(device)
            N = images.size(0)

            outputs = model(images)
            prediction= outputs.max(1, keepdim=True)[1]

            loss = criterion(outputs, labels)

            val_acc.update(prediction.eq(labels.view_as(prediction)).sum().item()/N)
            val_loss.update(loss.item())
            
            loop.set_postfix(loss=loss.item(), accuracy=val_acc.avg)

    val_stats.append([val_loss.avg, val_acc.avg])
    return val_loss.avg, val_acc.avg
