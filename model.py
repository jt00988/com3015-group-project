from torch import nn
from torchvision import models
def set_finetuning(model, is_finetuning):
    for param in model.parameters():
        param.required_grad = not is_finetuning

def get_model(name, num_classes, is_finetuning):
    if name == "resnet50":
        model = models.resnet50(pretrained=True)
        set_finetuning(model, is_finetuning)
        in_features = model.fc.in_features
        model.fc = nn.Linear(in_features, num_classes)
    elif name == "vgg11":
        model = models.vgg11_bn(pretrained=True)
        set_finetuning(model, is_finetuning)
        in_features = model.classifier[6].in_features
        model.classifier[6] = nn.Linear(in_features, num_classes)
    elif name == "densenet121":
        model = models.densenet121(pretrained=True)
        set_finetuning(model, is_finetuning)
        in_features = model.classifier.in_features
        model.classifier = nn.Linear(in_features, num_classes)
    elif name == "inception":
        model = models.inception_v3(pretrained=True)
        set_finetuning(model, is_finetuning)

        in_features = model.AuxLogits.fc.in_features
        model.AuxLogits.fc = nn.Linear(in_features, num_classes)
        in_features = model.fc.in_features
        model.fc = nn.Linear(in_features, num_classes)
    else:
        model = None
        print("No model with that name, things are about to go wrong!")
    return model