import torch
import torch.nn as nn

from constants import INPUT_DIM

class ConvModule(nn.Module):
    def __init__(self, in_channels, out_channels, pool=False, kernel_size=3):
        super().__init__()
        # Define your network architecture here
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, padding='same')
        self.relu = nn.ReLU(inplace=True)
        self.bn = nn.BatchNorm2d(out_channels)
        self.dropout = nn.Dropout2d(p=0.1)
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2) if pool else None

    def forward(self, x):
        x = self.conv(x)
        x = self.relu(x)
        x = self.bn(x)
        x = self.dropout(x)
        if self.pool: x = self.pool(x)
        return x

class DeconvModule(nn.Module):
    def __init__(self, in_channels, out_channels, depool=False, kernel_size=3):
        super().__init__()
        stride = 2 if depool else 1
        output_padding = 1 if depool else 0
        self.deconv = nn.ConvTranspose2d(in_channels, out_channels, kernel_size=kernel_size, stride=stride, padding=1, output_padding=output_padding)
        self.relu = nn.ReLU(inplace=True)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.dropout = nn.Dropout2d(p=0.1)

    def forward(self, x):
        x = self.deconv(x)
        x = self.relu(x)
        x = self.bn1(x)
        x = self.dropout(x)
        return x

class SegmentationModel(nn.Module):
    def __init__(self, channels: [int], pool: [bool]):
        super(SegmentationModel, self).__init__()
        # Encoder
        self.en1 =  ConvModule(3,16) #skip connection
        self.en2 = ConvModule(16, 32, True)
        self.en3 =  ConvModule(32, 64) #skip connection
        self.en4 =  ConvModule(64, 64, True)
        self.en5 =  ConvModule(64,128) #skip connection
        self.en6 =  ConvModule(128,128, True)
        self.en7 = ConvModule(128, 256) #skip connection
        self.en8 = ConvModule(256, 256, True)
        self.en9 = ConvModule(256, 512) #skip connection
        self.en10 = ConvModule(512, 512)

        #Decoder
        self.de1 = DeconvModule(512, 512)
        self.de2 = DeconvModule(512*2, 256)#skip connection
        self.de3 = DeconvModule(256, 256, True)
        self.de4 = DeconvModule(256*2, 128) #skip connection
        self.de5 = DeconvModule(128, 128, True)
        self.de6 = DeconvModule(128*2, 64)#skip connection
        self.de7 = DeconvModule(64, 64, True)
        self.de8 = DeconvModule(64*2, 32) #skip connection
        self.de9 = DeconvModule(32, 16, True)
        self.de10 = DeconvModule(16*2, 1)# skip connection

        #self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        # Perform forward pass of the network
        x = self.en1(x)
        x1 = x
        x = self.en2(x)
        x = self.en3(x)
        x3 = x
        x = self.en4(x)
        x = self.en5(x)
        x5 = x
        x = self.en6(x)
        x = self.en7(x)
        x7 = x
        x = self.en8(x)
        x = self.en9(x)
        x9 = x
        x = self.en10(x)


        x = self.de1(x)
        x = self.de2(torch.concat((x,x9),1))
        x = self.de3(x)
        x = self.de4(torch.concat((x,x7),1))
        x = self.de5(x)
        x = self.de6(torch.concat((x,x5),1))
        x = self.de7(x)
        x = self.de8(torch.concat((x,x3),1))
        x = self.de9(x)
        x = self.de10(torch.concat((x,x1),1))


        #x = self.sigmoid(x)
        return x
