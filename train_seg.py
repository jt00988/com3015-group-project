from typing import Callable
import torch.optim as optim
import torch.nn as nn
from torch.utils.data import DataLoader
import torch
import numpy as np
from tqdm import tqdm
import pandas as pd

from constants import INPUT_DIM

def jaccard_score(y_pred: torch.Tensor, y_true: torch.Tensor, threshold: float = 0.5) -> float:
    # Convert logits to probabilities using the sigmoid function
    y_pred = torch.sigmoid(y_pred.cpu())
    y_true = y_true.cpu().numpy()
    # Convert probabilities to binary mask using the threshold
    y_pred = (y_pred >= threshold).numpy()
    
    # Calculate the intersection and union of the two masks
    intersection = np.logical_and(y_pred, y_true)
    union = np.logical_or(y_pred, y_true)
    
    # Calculate the Jaccard score
    jaccard = np.sum(intersection) / np.sum(union)
    
    return jaccard

def accuracy(y_pred, y_true):
    correct = 0
    for pred, y in zip(y_pred.argmax(1).cpu().numpy(),y_true.cpu().numpy()):
        if pred == y: correct +=1
    return correct/len(y_true)

def train(model: nn.Module, criterion: nn.Module, optimizer: optim.Optimizer, loader: DataLoader, device: str, acccuracy_fn: Callable):
    epoch_loss = 0
    epoch_acc = 0
    model.train()

    loop = tqdm(loader, desc="\tTraining", ncols=100, mininterval=0.1)
    for x, y in loop:
        x, y = x.to(device), y.to(device)  # Move data to GPU if available
        optimizer.zero_grad()
        y_pred = model(x).float()
        loss = criterion(y_pred, y)
        loss.backward()
        optimizer.step()
        acc = acccuracy_fn(y_pred, y)
        epoch_loss += loss.item()
        epoch_acc += acc
        loop.set_postfix(loss = loss.item(), accuracy=acc)

    return epoch_loss / len(loader) , epoch_acc / len(loader)

def eval(model: nn.Module, criterion: nn.Module, loader: DataLoader, device: str, acccuracy_fn: Callable):
    epoch_loss = 0
    epoch_acc = 0
    model.eval()

    with torch.no_grad():
        loop = tqdm(loader, desc="\tValidation", ncols=100, mininterval=0.1)
        for x, y in loop:
            x, y = x.to(device), y.to(device)  # Move data to GPU if available
            y_pred = model(x)
            loss = criterion(y_pred, y)
            acc = acccuracy_fn(y_pred, y)
            epoch_loss += loss.item()
            epoch_acc += acc
            loop.set_postfix(loss = loss.item(), accuracy=acc)

    return epoch_loss / len(loader) , epoch_acc / len(loader)

def train_epochs(model: nn.Module, train_loader: DataLoader, val_loader: DataLoader, epochs: int):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    lr = 0.0001
    momentum = 0.9
    weight_decay = 0.0001
    nesterov = True
    optimizer = optim.SGD(model.parameters(), lr=lr, momentum=momentum, weight_decay=weight_decay, nesterov=nesterov)
    criterion = nn.BCEWithLogitsLoss()
    criterion.to(device)

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', verbose=True)

    best_val_loss = float('inf')
    train_metrics = {'train_loss': [], 'train_acc':[],  'val_loss': [], 'val_acc':[]}
    for epoch in range(1, epochs+1):
        print(f"Epoch: {epoch}")
        print("-"*20)

        train_loss, train_acc = train(model, criterion, optimizer, train_loader, device, jaccard_score)
        val_loss, val_acc = eval(model, criterion, val_loader, device, jaccard_score)

        print(f'\t     Train Loss: {train_loss:.3f},    Train Accuracy: {train_acc:.3f}')
        print(f'\tValidation Loss: {val_loss:.3f}, Validation Accuracy: {val_acc:.3f}')

        train_metrics['train_loss'].append(train_loss)
        train_metrics['train_acc'].append(train_acc)
        train_metrics['val_loss'].append(val_loss)
        train_metrics['val_acc'].append(val_acc)

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            torch.save(model.state_dict(), f"Models/segmentation{INPUT_DIM}skip.pt")

        scheduler.step(val_loss)

def train_epochs_classifier(model: nn.Module, train_loader: DataLoader, val_loader: DataLoader, epochs: int):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    lr = 0.0001
    momentum = 0.9
    weight_decay = 0.0001
    nesterov = True
    optimizer = optim.SGD(model.parameters(), lr=lr, momentum=momentum, weight_decay=weight_decay, nesterov=nesterov)
    criterion = nn.CrossEntropyLoss()
    criterion.to(device)

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', verbose=True)
    best_val_loss = float('inf')
    train_metrics = {'train_loss': [], 'train_acc':[],  'val_loss': [], 'val_acc':[]}
    for epoch in range(1, epochs+1):
        print(f"Epoch: {epoch}")
        print("-"*20)

        train_loss, train_acc = train(model, criterion, optimizer, train_loader, device, accuracy)
        val_loss, val_acc = eval(model, criterion, val_loader, device, accuracy)

        print(f'\t     Train Loss: {train_loss:.3f},    Train Accuracy: {train_acc:.3f}')
        print(f'\tValidation Loss: {val_loss:.3f}, Validation Accuracy: {val_acc:.3f}')

        train_metrics['train_loss'].append(train_loss)
        train_metrics['train_acc'].append(train_acc)
        train_metrics['val_loss'].append(val_loss)
        train_metrics['val_acc'].append(val_acc)

        scheduler.step(val_loss)

    model.train_seg = True

    best_val_loss = float('inf')
    for epoch in range(1, epochs+1):
        print(f"Epoch: {epoch}")
        print("-"*20)

        train_loss, train_acc = train(model, criterion, optimizer, train_loader, device, accuracy)
        val_loss, val_acc = eval(model, criterion, val_loader, device, accuracy)

        print(f'\t     Train Loss: {train_loss:.3f},    Train Accuracy: {train_acc:.3f}')
        print(f'\tValidation Loss: {val_loss:.3f}, Validation Accuracy: {val_acc:.3f}')

        train_metrics['train_loss'].append(train_loss)
        train_metrics['train_acc'].append(train_acc)
        train_metrics['val_loss'].append(val_loss)
        train_metrics['val_acc'].append(val_acc)

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            model.save()

        scheduler.step(val_loss)
    
    df = pd.DataFrame(data=train_metrics)
    df.to_csv("TrainStats/seg_classifier.csv")